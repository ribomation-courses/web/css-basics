const { src, dest, series } = require('gulp');
const del = require('delete');
const minifyHTML = require('gulp-htmlmin');

function clean(next) {
    del(['bld/*'], next);
}

function build() {
    return src('src/*.html')
        .pipe(minifyHTML({
            collapseWhitespace: true,
            html5: true,
            maxLineLength: 80,
            removeComments: true
        }))
        .pipe(dest('bld'));
}

exports.build = build;
exports.default = series(clean, build);


