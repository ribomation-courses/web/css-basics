const url  = 'https://baconipsum.com/api/?type=meat-and-filler';
const http = new XMLHttpRequest();
http.open('GET', url, false);
http.send();

const lorem = JSON.parse(http.responseText);
const ul    = document.createElement('ul');
lorem.forEach(txt => {
    const li = document.createElement('li');
    li.appendChild(document.createTextNode(txt));
    ul.appendChild(li);
});
document.querySelector('#lorem').appendChild(ul);

