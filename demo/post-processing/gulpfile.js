const { src, dest, series, parallel } = require('gulp');
const deleteFiles  = require('delete'),
      renameFile   = require('gulp-rename'),
      concatFiles  = require('gulp-concat'),
      concatCSS    = require('gulp-concat-css'),
      postCSS      = require('gulp-postcss'),
      minifyHTML   = require('gulp-htmlmin'),
      minifyImages = require('gulp-imagemin'),
      minifyJS     = require('gulp-uglify-es').default;

function css() {
    return src('src/css/*.css')
        .pipe(concatCSS('app.css'))
        .pipe(postCSS([
            require('postcss-color-rgba-fallback'),
            require('autoprefixer')({
                overrideBrowserslist: ['cover 99.5%'],
                flexbox: true,
                cascade: false,
    
            }),
            require('cssnano')
        ]))
        .pipe(dest('bld')) ;
}

function html() {
    return src('src/*.html')
        .pipe(minifyHTML({
            collapseWhitespace: true,
            html5: true,
            maxLineLength: 80,
            removeComments: true
        }))
        .pipe(dest('bld'));
}

function js() {
    return src('src/js/*.js')
        .pipe(concatFiles('app.js'))
        .pipe(minifyJS())
        .pipe(dest('bld'));
}

function img() {
    return src('src/img/*')
        .pipe(minifyImages([
            minifyImages.jpegtran({ progressive: true }),
            minifyImages.optipng()
        ], { verbose: true }
        ))
        .pipe(dest('bld/img'));
}

function clean(next) {
    deleteFiles(['bld/*'], next);
}

exports.css = css;
exports.default = series(clean, parallel(css, js, img, html));

