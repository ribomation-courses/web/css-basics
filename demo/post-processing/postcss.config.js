module.exports = {
    plugins: [
        require('postcss-color-rgba-fallback'),
        require('autoprefixer')({
            flexbox: true,
            overrideBrowserslist: ['cover 99.5%'],
            cascade: false,

        }),
        require('cssnano'),
    ]
}


