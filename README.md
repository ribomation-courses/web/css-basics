# CSS for Programmers
## 4 days


Welcome to this course.
The syllabus can be found at
[web/css-basics](https://www.ribomation.se/courses/web/css-basics.html)

Here you will find
* Installation instructions
* Solutions to the programming exercises
* Source code of the demo programs

_N.B. The solutions and demo folders will be populated during the course._

# Installation Instructions

In order to do the programming exercises of the course, you need to have
the following installed.

* A modern browser, such as
  - [Google Chrome](https://www.google.com/chrome/browser/desktop/index.html)
  - [Mozilla FireFox](https://www.mozilla.org/sv-SE/firefox/new/)
  - [MicroSoft Edge](https://www.microsoft.com/sv-se/windows/microsoft-edge)
* A decent IDE, such as
    * MicroSoft Visual Code
        - https://code.visualstudio.com/
    * JetBrains WebStorm
        - https://www.jetbrains.com/webstorm/download
* A BASH terminal, such as the GIT client
  - [GIT Client](https://git-scm.com/downloads)
* NodeJS / NPM
  - https://nodejs.org/en/download/

# Usage of this GIT Repo

You need to have a GIT client installed to clone this repo. Otherwise, you can just click on the download button and grab it all as a ZIP or TAR bundle.

Get the sources initially by a git clone operation

    mkdir -p ~/css-course/my-solutions
    cd ~/css-course
    git clone https://gitlab.com/ribomation-courses/web/css-basics.git gitlab
    

Get the latest updates by a git pull operation

    cd ~/css-course/gitlab
    git pull

Put your own solutions into `~/css-course/my-solutions`, create one sub-folder per project.

# Running the solution and demo apps

If the solutions folder contains a `package.json` file,
run first the following command inside the chosen folder.

    npm install

***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>
