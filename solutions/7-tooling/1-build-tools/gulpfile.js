const { src, dest, series, parallel } = require('gulp');
const minHTML = require('gulp-htmlmin');
const minCSS = require('gulp-clean-css');
const del = require('delete');

function clean(next) {
    del(['./bld/*'], next);
}

function css() {
    return src('./src/*.css')
        .pipe(minCSS({
            level: 2
        }))
        .pipe(dest('./bld'))
        ;
}
function html() {
    return src('./src/*.html')
        .pipe(minHTML({
            collapseWhitespace: true,
            html5: true,
            removeComments: true,
            maxLineLength: 60,
        }))
        .pipe(dest('./bld'))
        ;
}

exports.default = series(clean, parallel(css, html));

