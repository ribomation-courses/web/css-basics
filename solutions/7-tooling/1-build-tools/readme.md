# Setup this Project

    npm init --yes
    npm install --save-dev gulp gulp-cli gulp-htmlmin gulp-clean-css delete local-web-server
    touch gulpfile.js
    mkdir src bld

Copy `*.hmtl` and `*.css` files into the `./src/` folder.

Edit `gulpfile.js`, to the following content:

    const { src, dest, series, parallel } = require('gulp');
    const minHTML = require('gulp-htmlmin');
    const minCSS = require('gulp-clean-css');
    const del = require('delete');

    function clean(next) {
        del(['./bld/*'], next);
    }
    function css() {
        return src('./src/*.css')
            .pipe(minCSS({
                level: 2
            }))
            .pipe(dest('./bld'))
            ;
    }
    function html() {
        return src('./src/*.html')
            .pipe(minHTML({
                collapseWhitespace: true,
                html5: true,
                removeComments: true,
                maxLineLength: 60,
            }))
            .pipe(dest('./bld'))
            ;
    }
    exports.default = series(clean, parallel(css, html));

Add the following to the `scripts` of `./package,json`

    "build": "npx gulp",
    "launch": "npx ws --directory ./bld --open"

Finally, run it

    npx run build
    npx run launch


