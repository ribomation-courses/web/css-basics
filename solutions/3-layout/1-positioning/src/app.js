
document
    .querySelector('.show')
    .addEventListener('click', (ev) => {
        ev.preventDefault();
        const pane = document.querySelector('.msg');

        pane.classList.add('msg--show');
        setTimeout(() => {
            pane.classList.remove('msg--show');
        }, 4000);
    });
