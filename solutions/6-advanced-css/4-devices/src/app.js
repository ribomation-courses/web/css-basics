function reportSize() {
    const width = window.innerWidth;
    const sizePane = document.querySelector('#size');
    sizePane.innerText = `${width}px`;
}

window.addEventListener('resize', reportSize);

reportSize();
