
document
    .querySelector('#showToast')
    .addEventListener('click', ev => {
        const toast = document.querySelector('.toast');

        toast.classList.add('shown');
        setTimeout(() => {
            toast.classList.remove('shown');
        }, 3000);
    });
