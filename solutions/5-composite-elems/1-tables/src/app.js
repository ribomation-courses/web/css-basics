const invoice = {
    id: '123-987665',
    date: new Date().toString().substr(0, 15), 
    payTo: {
        name: 'Whatever Ltd.',
        street: '42 Reboot Lane',
        city: 'Perl Lake',
        zip: 'NG 999'
    },
    customer: {
        name: 'Nisse Hult',
        street: '17 Hacker Street',
        city: 'Javaville',
        zip: 'PWA 1234'
    },
    items: [
        { name: 'Apple', count: 1, price: 1.25 },
        { name: 'Banana', count: 3, price: 0.45 },
        { name: 'Coco Nut', count: 1, price: 1.75 },
        { name: 'Date Plum', count: 5, price: 0.3 },
    ]
};

function currency(amount) {
    return Number(amount)
        .toLocaleString('sv', {
            useGrouping: true,
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
        })
        + '&nbsp;kr'
        ;
}

function populateHead(invoice, tbl) {
    const invoiceNo = tbl.querySelector('thead tr:first-child span');
    invoiceNo.innerHTML = invoice.id;

    const invoiceDate = tbl.querySelector('thead tr:first-child th:last-child');
    invoiceDate.innerHTML = invoice.date;

    const payTo = tbl.querySelector('thead tr:nth-child(2) td:first-child address');
    payTo.innerHTML =
        `${invoice.payTo.name}<br>
         ${invoice.payTo.street}<br>
         ${invoice.payTo.city} ${invoice.payTo.zip} 
        `;

    const cust = tbl.querySelector('thead tr:nth-child(2) td:last-child address');
    cust.innerHTML =
        `${invoice.customer.name}<br>
         ${invoice.customer.street}<br>
         ${invoice.customer.city} ${invoice.customer.zip} 
        `;
}

function populateBody(invoice, tbl) {
    const tbody = tbl.querySelector('tbody');
    invoice.items.forEach(item => {
        const tr = document.createElement('tr');
        tr.innerHTML = `
        <td>${item.name}</td>
        <td>${item.count}</td>
        <td>${currency(item.price)}</td>
        <td>${currency(item.count * item.price)}</td> `;
        tbody.appendChild(tr);
    });
}

function populateFoot(invoice, tbl) {
    const subTotal = invoice.items
        .map(item => item.count * item.price)
        .reduce((sum, cost) => sum + cost, 0);

    const vat = 25;
    const vatAmount = subTotal * vat / 100;
    const grandTotal = subTotal + vatAmount;

    const subTotalElem = tbl.querySelector('tfoot tr:nth-child(1) td:last-child');
    subTotalElem.innerHTML = currency(subTotal);

    const vatElem = tbl.querySelector('tfoot tr:nth-child(2) td:nth-of-type(1)');
    vatElem.innerHTML = vat + '%';

    const vatAmountElem = tbl.querySelector('tfoot tr:nth-child(2) td:nth-of-type(2)');
    vatAmountElem.innerHTML = currency(vatAmount);

    const grandTotalElem = tbl.querySelector('tfoot tr:nth-child(3) td:nth-of-type(1)');
    grandTotalElem.innerHTML = currency(grandTotal);
}


function populate(invoice, tbl) {
    populateHead(invoice, tbl);
    populateBody(invoice, tbl);
    populateFoot(invoice, tbl);
}

populate(invoice, document.querySelector('table'));

