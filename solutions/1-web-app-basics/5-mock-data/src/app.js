
function loadLorem(pane) {
    const url = 'https://baconipsum.com/api/?type=meat-and-filler';
    const http = new XMLHttpRequest();
    http.open('GET', url, false);
    http.send();
    const txt = JSON.parse(http.responseText);
    const ul = document.createElement('el');
    txt.forEach(p => {
        const li = document.createElement('li');
        li.appendChild(document.createTextNode(p));
        ul.appendChild(li);
    });
    pane.appendChild(ul);
}

loadLorem(document.querySelector('#lorem'));


